﻿using System;

namespace Input_Output
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Введите имя...");
                string name = Console.ReadLine();

                Console.WriteLine("Введите возраст...");
                var age = Convert.ToByte(Console.ReadLine());

                Console.WriteLine("Введите профессию...");
                var profession = Console.ReadLine();

                Console.WriteLine("Введите любимый цвет...");
                var color = Console.ReadLine();

                Console.WriteLine("Введите рост (m)...");
                var height = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("----");
                Console.WriteLine(name);
                Console.WriteLine(age);
                Console.WriteLine(profession);
                Console.WriteLine(color);
                Console.WriteLine(height);
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка ввода, попробуйте ещё раз...");
            }
        }
    }
}