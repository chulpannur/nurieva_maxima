﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GenStackClassLibrary
{
    public class GenStack<T>
    {
        private List<T> list;

        public GenStack(int capacity)
        {
            list = new List<T>(capacity);
        }
        public void Push(T item)
        {
            list.Add(item);
        }

        public T Pop()
        {
            var item = list.LastOrDefault();
            list.RemoveAt(list.Count-1);
            return item;
        }
        
        public T Peek()
        {
            var item = list.LastOrDefault();
            return item;
        }

    }
}