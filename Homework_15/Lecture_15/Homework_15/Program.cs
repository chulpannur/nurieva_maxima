﻿using System;
using System.Collections.Generic;
using GenStackClassLibrary;

namespace Homework_15
{
    class Program
    {
        static void Main(string[] args)
        {
            var genStack = new GenStack<int>(10);

            Random random = new Random();
            //Push items
            for (int i = 0; i < 5; i++)
            {
                genStack.Push(random.Next(1,10));
            }
            //Fist item
            Console.WriteLine($"Fist item : {genStack.Peek()}");
            
            // Get all items and print in console
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine(genStack.Pop());
            }

        }
    }
}