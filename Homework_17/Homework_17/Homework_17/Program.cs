﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Homework_17
{
    class Program
    {
        static void Main(string[] args)
        {
            int k=4;
            List<int> a = new List<int>(10);
            Random random = new Random();
            
            for (int i = 0; i < 10; i++)
            {
                a.Add(random.Next(1, 100));
            }
            Console.WriteLine("Исходный массив A");
            foreach (var item in a)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("Задание 1");
            
            var query = a
                .Where(x => x % 2 == 0)
                .Except(a.Skip(k))
                .Distinct()
                .Reverse()
                .ToList();

            foreach (var item in query)
            {
                Console.WriteLine(item);
            }

            int k1 = 3;
            int k2 = 5;
            List<int> b = new List<int>(10);

            for (int i = 0; i < 10; i++)
            {
                b.Add(random.Next(1, 100));
            }
            
            Console.WriteLine("Исходный массив B");
            foreach (var item in b)
            {
                Console.WriteLine(item);
            }
            
            Console.WriteLine("Задание 2");
            
            var result = a
                .Where(x => x > k1)
                .Union(b.Where(q => q < k2))
                .OrderBy(order => order);
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
            
            Console.WriteLine("Задание 3");
            
            List<Abiturient> abiturients = new List<Abiturient>();
            abiturients.Add(new Abiturient{LastName = "Popov",Year = 2020,  SchoolNumber = 54});
            abiturients.Add(new Abiturient{LastName = "Nurieva",Year = 2020,SchoolNumber = 34});
            abiturients.Add(new Abiturient{LastName = "Ivanov",Year = 2021, SchoolNumber = 3});
            abiturients.Add(new Abiturient{LastName = "Nuriev",Year = 2020, SchoolNumber = 3});

            var res = abiturients
                .OrderBy(order=>order.Year)
                .GroupBy(group => group.SchoolNumber)
                .ToList()
                .OrderBy(order=>order.Key)
                .Select(s => new
                {
                    SchoolNumber = s.Key,
                    Count = s.Count(),
                    Lastname = s.Select(f => f.LastName).First()
                });
            
            foreach (var item in res)
            {
                Console.WriteLine($" School number : {item.SchoolNumber} Count : {item.Count} LastName : {item.Lastname}");
            }
        }
    }
}