﻿using System;

namespace ArrayClassLibrary
{
    public static class IntArrayExtensions
    {
        public static void ReplaceValue(this int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < 0)
                {
                    array[i] = 0;
                }
            }
        }

        public static int[] ReverseArray(this int[] array)
        {
            int j = 0;
            int[] temp = new int[array.Length];
            for (int i = array.Length - 1; i >= 0; i--)
            {
                temp[j] = array[i];
                j++;
            }

            return temp;
        }

        public static void PrintArray(this int[] array)
        {
            foreach (var i in array)
            {
                Console.WriteLine(i);
            }
        }
    }
}