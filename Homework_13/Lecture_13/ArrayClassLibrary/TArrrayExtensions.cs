﻿using System;

namespace ArrayClassLibrary
{
    public static class TArrrayExtensions
    {
        public static void MixArray<T>(this T[] array)
        {
            Random random = new Random();
            for (int i = array.Length - 1; i >= 1; i--)
            {
                int j = random.Next(i + 1);
                var temp = array[j];
                array[j] = array[i];
                array[i] = temp;
            }
        }
    }
}