﻿using System;
using ArrayClassLibrary;
using TVRemoteClassLibrary;

namespace Homework_13
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            int[] intArray = new int[10];

            for (int i = 0; i < intArray.Length; i++)
            {
                intArray[i] = random.Next(-10,20);
            }
            intArray.PrintArray();
            
            Console.WriteLine("Replace negative values with zero");
            intArray.ReplaceValue();
            intArray.PrintArray();
            
            Console.WriteLine("Reverse array");
            intArray.ReverseArray().PrintArray();

            var tvRemote = new TVRemote();
            tvRemote.TurnOnTV();
            tvRemote.TurnOffTV();
            
            Console.WriteLine("Mix array");
            intArray.MixArray();
            intArray.PrintArray();

        }
    }
}