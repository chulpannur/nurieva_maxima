﻿using System;

namespace TVRemoteClassLibrary
{
    public partial class TVRemote
    {
        public bool TVIsOn { get; private set; }

        public partial void TurnOnTV()
        {
            TVIsOn = true;
            Console.WriteLine("TV is turned on");
        }

        public partial void TurnOffTV();

    }
    
    public partial class TVRemote
    {
        public partial void TurnOnTV();

        public partial void TurnOffTV()
        {
            TVIsOn = false;
            Console.WriteLine("TV is turned off");
        }
    }
}