﻿using System;

namespace Homework_8
{
    class Program
    {
        static void Main(string[] args)
        {
            var genListInt = new GenericList<int>(4);
            genListInt.Add(1);
            genListInt.Add(3);
            genListInt.Add(4);
            genListInt.Add(3);
            genListInt.Add(3);

            genListInt.Print();
            
            var genListStr = new GenericList<string>(1);
            genListStr.Add("item1");
            
            genListStr.Print();

            var genListChild = new GenericListChild(1);
            genListChild.Add(2);
            genListChild.Print();

            var printer = new Printer();
            printer.Print(genListChild);
        }
    }
}