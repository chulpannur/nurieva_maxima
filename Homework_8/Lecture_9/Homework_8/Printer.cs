using System;

namespace Homework_8
{
    public class Printer
    {
        public void Print<T>(T item) where T: class
        {
           Console.WriteLine(item.ToString());
        }
    }
}