using System;

namespace Homework_8
{
    public class GenericList<T>
    {
        private T[] array;
        private int index;

        public GenericList(int count)
        {
            array = new T[count];
        }


            public void Add(T item)
            {
                if (index == array.Length)
                {
                    Console.WriteLine("error");
                    return;
                }
                array[index] = item;
                index++;
            }

            public void Print()
            {
                for (int i = 0; i < array.Length; i++)
                {
                    Console.WriteLine(array[i]);
                }
                
            }
        
    }
}