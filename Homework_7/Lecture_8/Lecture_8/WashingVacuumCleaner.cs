using System;

namespace Lecture_8
{
    public class WashingVacuumCleaner: VacuumCleaner
    {
        public override string Model => "Washing";

        public new void StartCleaning()
        {
            Console.WriteLine($"{Model} started cleaning...");
        }
    }
}