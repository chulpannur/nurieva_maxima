using System;

namespace Lecture_8
{
    public class VacuumCleaner
    {
        public virtual string Model { get; set; }
        
        public virtual void StartCleaning()
        {
            Console.WriteLine($"{Model} started cleaning");
        }
        public virtual void StartCleaning(string place)
        {
            Console.WriteLine($"Cleaning started in {place}");
        }
        
    }
}