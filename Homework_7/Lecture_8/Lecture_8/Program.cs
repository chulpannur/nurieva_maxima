﻿using System;

namespace Lecture_8
{
    class Program
    {
        static void Main(string[] args)
        {
            VacuumCleaner[] vacuumCleaners = new VacuumCleaner[3];
            vacuumCleaners[0] = new RegularVacuumCleaner();
            vacuumCleaners[1] = new RobotVacuumCleaner();
            vacuumCleaners[2] = new WashingVacuumCleaner();

            for (int i = 0; i < vacuumCleaners.Length; i++)
            {
                vacuumCleaners[i].StartCleaning();
            }
        }
    }
}