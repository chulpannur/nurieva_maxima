using System;

namespace Lecture_8
{
    public class RobotVacuumCleaner: VacuumCleaner
    {
        public override string Model { get; set; }
        
        public override void StartCleaning()
        {
            Model = "Robot";
            base.StartCleaning();
        }
    }
}