﻿namespace Homework_12
{
    public class TransaportCard
    {
        private int currentBalance = 200; 
        
        public delegate void BalanceState(int currentBalance);

        public event BalanceState OnBalanceOperation;
        
        public event BalanceState OnInsufficientFunds ;

        public void TopUp(int amount)
        {
            currentBalance += amount;
            if (OnBalanceOperation != null) 
                OnBalanceOperation.Invoke(currentBalance);
        }

        public void Pay()
        {
            if (currentBalance > 30)
            {
                currentBalance -= 30;
                if (OnBalanceOperation != null) 
                    OnBalanceOperation.Invoke(currentBalance);
            }

            else
            {
                if (OnBalanceOperation != null)
                    OnInsufficientFunds.Invoke(currentBalance);
            }
        }
    }
}
