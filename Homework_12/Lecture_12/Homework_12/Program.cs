﻿using System;

namespace Homework_12
{
    class Program
    {
        static void Main(string[] args)
        {
            var transaportCard = new TransaportCard();
            transaportCard.OnBalanceOperation+= TransaportCardOnOnBalanceOperation;
            transaportCard.OnInsufficientFunds+= currentBalance=>
                Console.WriteLine($"Balance : {currentBalance}, operation is failed, insufficient funds");;
            
            for (int i = 0; i < 7; i++)
            {
                Console.WriteLine("Payment by card 30 rub");
                transaportCard.Pay();
                
            }

            Console.WriteLine("Card replenishment");
            transaportCard.TopUp(40);
            
        }

        private static void TransaportCardOnOnBalanceOperation(int currentBalance)
        {
            Console.WriteLine($"Balance : {currentBalance}");
        }
    }
}