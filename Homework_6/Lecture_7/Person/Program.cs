﻿using System;
using System.Security.Cryptography;

namespace Person
{
    public enum Color
    {
        Brown,
        Green,
        Blue,
        Grey
    }
    public class Person
    {
        private string name;
        private byte age;
        private double height;
        private double weight;
        private Color color;

        public byte Age
        {
            get => age;
            set
            {
                if (value>0 && value<=120)
                {
                    age = value;
                }
                else Console.WriteLine("Введите положительное число (не более 120)");
            }
        }
        public double Height
        {
            get
            {
                return height;
            }
            set
            {
                if (value>0 && value<=3)
                {
                    height = value;
                }
                else Console.WriteLine("Введите положительное число (не более 3 м)");
            }
        }
        public double Weight
        {
            get
            {
                return weight;
            }
            set
            {
                if (value>0)
                {
                    weight = value;
                }
                else Console.WriteLine("Введите положительное число");
            }
        }

        public Person(string name, byte age, double height, double weight, Color color)
        {
            this.name=name;
            Age = age;
            Height = height;
            Weight = weight;
            this.color = color;
        } 

        public void Print()
        {
           Console.WriteLine($"Имя:{name}, Возраст:{age}, Рост:{height}, " +
                             $"Вес:{weight}, Цвет глаз:{color}"); 
        }


    }
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Введите имя:");
                var name = Console.ReadLine();

                Console.WriteLine("Введите возраст:");
                var age = Convert.ToByte(Console.ReadLine());

                Console.WriteLine("Введите рост (m):");
                var height = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("Введите вес:");
                var weight =Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("Введите цвет глаз (0-3): 0 - Brown, 1 - Green, 2 - Blue, 3 - Grey");
                var color =Convert.ToInt16(Console.ReadLine());

                var person = new Person(name, age, height, weight, (Color)color);
                person.Print();
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка ввода, попробуйте ещё раз...");
            }
        }
    }
}