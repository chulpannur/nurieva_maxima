﻿using System;

namespace Array
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input 10 numbers");
            try
            {
                int[] array = new int[10];
                int oddSum = 0, evenSum = 0;

                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = int.Parse(Console.ReadLine());
                    (evenSum,oddSum) = GetPositiveEvenOddSum(array[i], ref evenSum, ref oddSum);
                }

                if (oddSum > 0 || evenSum > 0)
                {
                    var res = GetResult(oddSum, evenSum);
                    Console.WriteLine($"Result: {res}");
                }
                else
                    Console.WriteLine("No positive numbers");
                
                Console.WriteLine("Array in reverse order:");
                GetArrayInReverse(array);
            }
            catch( Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private static int GetResult(int oddSum, int evenSum) 
            => evenSum - oddSum;

        private static void GetArrayInReverse(int[] array)
        {
            for (int i = array.Length-1; i >= 0; i--)
            {
                Console.Write(array[i]+" ");
            }
        }

        private static (int,int) GetPositiveEvenOddSum(int number, ref int evenSum, ref int oddSum)
        {
            if (number> 0)
            {
                if (number % 2 == 0)
                {
                    evenSum += number;
                }
                else
                {
                    oddSum += number;
                }
            }
            return (evenSum,oddSum);
        }
        
    }
}
