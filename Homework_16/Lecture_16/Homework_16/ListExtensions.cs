﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Homework_16
{
    public static class ListExtensions
    {
        public static void GetSortedList(this List<int> list)
        {
            var query = 
                list.Where(i => i % 2 != 0)
                .Distinct();
            foreach (var item in query)
            {
                Console.WriteLine(item);
            }
        }

        public static void GetParsedList(this List<int> list)
        {
            var query =
                list.Where(i => i % 2 != 0)
                    .OrderBy(order => order)
                    .ToList()
                    .Select(s => s.ToString());
            foreach (var item in query)
            {
                Console.WriteLine(item);
            }
        }
    }
}