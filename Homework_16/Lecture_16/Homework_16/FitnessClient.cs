﻿namespace Homework_16
{
    public class FitnessClient
    {
        public int ClientdId { get; set; }
        
        public int Year { get; set; }
        
        public int Month { get; set; }
        
        public int Duration { get; set; }
    }
}