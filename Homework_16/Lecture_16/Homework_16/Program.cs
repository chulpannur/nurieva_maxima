﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Homework_16
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> list = new List<int>(10);
            Random random = new Random();

            for (int i = 0; i < 10; i++)
            {
               list.Add(random.Next(-10, 10));
            }
            list.GetSortedList(); //задание 1
            list.Clear();
            Console.WriteLine("*****");
            for (int i = 0; i < 10; i++)
            {
                list.Add(random.Next(1, 10));
            }
            
            list.GetParsedList(); //задание 2

            List<FitnessClient> fitnessClients = new List<FitnessClient>();
            fitnessClients.Add(new FitnessClient{ClientdId = 12,Year = 2020,Month = 10,Duration = 54});
            fitnessClients.Add(new FitnessClient{ClientdId = 123,Year = 2020,Month = 10,Duration = 34});
            fitnessClients.Add(new FitnessClient{ClientdId = 143,Year = 2021,Month = 5,Duration = 3});
            fitnessClients.Add(new FitnessClient{ClientdId = 136,Year = 2020,Month = 4,Duration = 3});
            fitnessClients.Add(new FitnessClient{ClientdId = 167,Year = 2021,Month = 11,Duration = 37});
            
            Console.WriteLine("*******");
            var query = fitnessClients
                .Where(last => last
                    .Duration == fitnessClients
                    .Select(s => s.Duration)
                    .Min())
                .Select(s => $"Продолжительность : {s.Duration} Год : {s.Year} Месяц: {s.Month}")
                .LastOrDefault(); //задание 3
            
            Console.WriteLine(query);

        }
    }
}