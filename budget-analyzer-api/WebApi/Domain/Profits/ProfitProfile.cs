using AutoMapper;
using DataAccessLayer.Models;

namespace WebApi.Domain.Profits
{
    public class ProfitProfile : Profile
    {
        public ProfitProfile()
        {
            CreateMap<ProfitDto, Profit>();
            CreateMap<Profit, ProfitDto>();
        }
    }
}