using System;
using System.Threading;
using System.Threading.Tasks;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.Profits.Commands
{
    public class UpdateProfitCommand : IRequest<bool>
    {
        public int Id { get; }
        public DateTime ProfitDateTime { get; set; }
        public double Amount { get; set; }
    }
    
    public class UpdateCategoryCommandValidator : AbstractValidator<UpdateProfitCommand>
    {
        public UpdateCategoryCommandValidator()
        {
            RuleFor(profit => profit.Id)
                .NotEmpty()
                .GreaterThanOrEqualTo(1);
            RuleFor(profit => profit.ProfitDateTime)
                .NotEmpty();
            RuleFor(profit => profit.Amount)
                .NotEmpty();
        }
    }

    public class UpdateProfitCommandHadler : IRequestHandler<UpdateProfitCommand, bool>
    {
        private readonly DataContext _dataContext;

        public UpdateProfitCommandHadler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<bool> Handle(UpdateProfitCommand request, CancellationToken cancellationToken)
        {
            var profit = await _dataContext.Profits
                .FirstOrDefaultAsync(p => p.Id == request.Id, cancellationToken: cancellationToken);
            if (profit == null)
                return false;
            
            profit.ProfitDateTime = request.ProfitDateTime;
            profit.Amount = request.Amount;
            
            await _dataContext.SaveChangesAsync(cancellationToken);
            return true;

        }
    }
}