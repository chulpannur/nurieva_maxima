using System;
using System.Threading;
using System.Threading.Tasks;
using DataAccessLayer;
using DataAccessLayer.Models;
using FluentValidation;
using MediatR;

namespace WebApi.Domain.Profits.Commands
{
    public class CreateProfitCommand : IRequest
    {
        public DateTime ProfitDateTime { get; }
        public double Amount { get; }
        public int MoneyAccountId { get; }
        public int UserId { get; }
        
        public CreateProfitCommand(DateTime profitDateTime, double amount, int moneyAccountId, int userId)
        {
            ProfitDateTime = profitDateTime;
            Amount = amount;
            MoneyAccountId = moneyAccountId;
            UserId = userId;
        }
    }
    
    public class CreateProfitCommandValidator : AbstractValidator<CreateProfitCommand>
    {
        public CreateProfitCommandValidator()
        {
            RuleFor(profit => profit.ProfitDateTime)
                .NotEmpty();
            RuleFor(profit => profit.Amount)
                .NotEmpty();
            RuleFor(profit => profit.MoneyAccountId)
                .NotEmpty();
            RuleFor(profit => profit.UserId)
                .NotEmpty();
        }
    }

    public class CreateProfitCommandHadler : IRequestHandler<CreateProfitCommand>
    {
        private readonly DataContext _dataContext;

        public CreateProfitCommandHadler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Unit> Handle(CreateProfitCommand request, CancellationToken cancellationToken)
        {
            await _dataContext.AddAsync(new Profit
            {
                ProfitDateTime = request.ProfitDateTime,
                Amount = request.Amount,
                MoneyAccountId = request.MoneyAccountId,
                UserId = request.UserId
            }, cancellationToken);

            await _dataContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}