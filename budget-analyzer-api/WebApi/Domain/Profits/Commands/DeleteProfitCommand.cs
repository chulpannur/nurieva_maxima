using System.Threading;
using System.Threading.Tasks;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.Profits.Commands
{
    public class DeleteProfitCommand : IRequest<bool>
    {
        public int Id { get;}

        public DeleteProfitCommand(int id)
        {
            Id = id;
        }
    }
    
    // public class DeleteProfitCommandValidator : AbstractValidator<DeleteProfitCommand>
    // {
    //     public DeleteProfitCommandValidator()
    //     {
    //         RuleFor(profit => profit.Id)
    //             .NotEmpty()
    //             .GreaterThanOrEqualTo(1);
    //     }
    // }

    public class DeleteProfitCommandHadler : IRequestHandler<DeleteProfitCommand, bool>
    {
        private readonly DataContext _dataContext;

        public DeleteProfitCommandHadler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<bool> Handle(DeleteProfitCommand request, CancellationToken cancellationToken)
        {
            var profit = await _dataContext.Profits
                .FirstOrDefaultAsync(p => p.Id == request.Id, cancellationToken: cancellationToken);
            if (profit == null)
            {
                return false;
            }
            _dataContext.Profits.Remove(profit);
            await _dataContext.SaveChangesAsync(cancellationToken);
            
            return true;
            

        }
    }
}