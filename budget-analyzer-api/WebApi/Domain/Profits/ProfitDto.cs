using System;
using WebApi.Domain.Users;
using MoneyAccountDto = WebApi.Domain.MoneyAccounts.MoneyAccountDto;

namespace WebApi.Domain.Profits
{
    public class ProfitDto
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Дата дохода
        /// </summary>
        public DateTime ProfitDateTime { get; set; }
        
        /// <summary>
        /// Сумма дохода
        /// </summary>
        public double Amount { get; set; }
        
        /// <summary>
        /// Счет
        /// </summary>
        public MoneyAccountDto MoneyAccount { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public UserDto User { get; set; }
    }
}