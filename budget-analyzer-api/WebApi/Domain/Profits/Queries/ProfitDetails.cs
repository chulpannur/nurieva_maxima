using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.Profits.Queries
{
    public class ProfitDetails : IRequest<ProfitDto>
    {
        public int Id { get; }

        public ProfitDetails(int id)
        {
            Id = id;
        }
    }
    
    public class ProfitDetailsHadler : IRequestHandler<ProfitDetails, ProfitDto>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public ProfitDetailsHadler(IMapper mapper, DataContext dataContext)
        {
            _mapper = mapper;
            _dataContext = dataContext;
        }

        public async Task<WebApi.Domain.Profits.ProfitDto> Handle(ProfitDetails request, CancellationToken cancellationToken)
        {
            var profit = await _dataContext.Expenses
                .FirstOrDefaultAsync(profit => profit.Id == request.Id, cancellationToken: cancellationToken);
                
            return _mapper.Map<WebApi.Domain.Profits.ProfitDto>(profit);
        }
    }
}