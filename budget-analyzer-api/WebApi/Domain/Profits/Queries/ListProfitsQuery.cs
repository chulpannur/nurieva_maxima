using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.Profits.Queries
{
    public class ListProfitsQuery : IRequest<IEnumerable<ProfitDto>>
    {
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    
    public class ListProfitsQueryValidator : AbstractValidator<ListProfitsQuery>
    {
        public ListProfitsQueryValidator()
        {
            RuleFor(profit => profit.Skip)
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(100);
            RuleFor(profit => profit.Take)
                .NotEmpty()
                .GreaterThanOrEqualTo(1)
                .LessThanOrEqualTo(100);
        }
    }
    
    public class ListProfitsQueryHadler : IRequestHandler<ListProfitsQuery, IEnumerable<ProfitDto>>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public ListProfitsQueryHadler(IMapper mapper, DataContext dataContext)
        {
            _mapper = mapper;
            _dataContext = dataContext;
        }

        public async Task<IEnumerable<ProfitDto>> Handle(ListProfitsQuery request, CancellationToken cancellationToken)
        {
            var profitData = await _dataContext.Profits
                .Skip(request.Skip)
                .Take(request.Take)
                .Include(profit => profit.User)
                .Include(account => account.MoneyAccount)
                .AsNoTracking()
                .ToListAsync(cancellationToken: cancellationToken);

            return _mapper.Map<IEnumerable<ProfitDto>>(profitData);
        }
    }
}