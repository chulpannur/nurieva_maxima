using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.Profits.Queries
{
    public class ListProfitsForPeriodQuery : IRequest<IEnumerable<ProfitDto>>
    {
        public int Skip { get; set; }
        public int Take { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
    
    public class ListProfitsForPeriodQueryValidator : AbstractValidator<ListProfitsForPeriodQuery>
    {
        public ListProfitsForPeriodQueryValidator()
        {
            RuleFor(profit => profit.Skip)
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(100);
            RuleFor(profit => profit.Take)
                .NotEmpty()
                .GreaterThanOrEqualTo(1)
                .LessThanOrEqualTo(100);
            RuleFor(profit => profit.FromDate)
                .NotEmpty();
            RuleFor(profit => profit.ToDate)
                .NotEmpty();
        }
    }
    
    public class ListProfitsForPeriodQueryHadler : IRequestHandler<ListProfitsForPeriodQuery, IEnumerable<ProfitDto>>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public ListProfitsForPeriodQueryHadler(IMapper mapper, DataContext dataContext)
        {
            _mapper = mapper;
            _dataContext = dataContext;
        }

        public async Task<IEnumerable<ProfitDto>> Handle(ListProfitsForPeriodQuery request, CancellationToken cancellationToken)
        {
            var profitsData = await _dataContext.Profits
                .Skip(request.Skip)
                .Take(request.Take)
                .Where(profit => profit.ProfitDateTime >= request.FromDate 
                                 && profit.ProfitDateTime<=request.ToDate)
                .Include(profit => profit.MoneyAccount)
                .AsNoTracking()
                .ToListAsync(cancellationToken: cancellationToken);
            
            return _mapper.Map<IEnumerable<ProfitDto>>(profitsData);
        }
    }
}