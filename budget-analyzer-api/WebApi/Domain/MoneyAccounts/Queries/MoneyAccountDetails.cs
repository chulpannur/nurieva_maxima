using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.MoneyAccounts.Queries
{
    public class MoneyAccountDetails : IRequest<MoneyAccountDto>
    {
        public int Id { get; }

        public MoneyAccountDetails(int id)
        {
            Id = id;
        }
    }
    
    public class MoneyAccountDetailsHadler : IRequestHandler<MoneyAccountDetails, MoneyAccountDto>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public MoneyAccountDetailsHadler(IMapper mapper, DataContext dataContext)
        {
            _mapper = mapper;
            _dataContext = dataContext;
        }

        public async Task<MoneyAccountDto> Handle(MoneyAccountDetails request, CancellationToken cancellationToken)
        {
            var moneyAccount = await _dataContext.MoneyAccounts
                .FirstOrDefaultAsync(account => account.Id == request.Id, cancellationToken: cancellationToken);
                
            return _mapper.Map<MoneyAccountDto>(moneyAccount);
        }
    }
}