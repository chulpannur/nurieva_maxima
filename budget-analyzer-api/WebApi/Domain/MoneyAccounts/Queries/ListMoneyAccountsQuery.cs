using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.MoneyAccounts.Queries
{
    public class ListMoneyAccountsQuery : IRequest<IEnumerable<MoneyAccountDto>>
    {
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    
    public class ListMoneyAccountsQueryValidator : AbstractValidator<ListMoneyAccountsQuery>
    {
        public ListMoneyAccountsQueryValidator()
        {
            RuleFor(account => account.Skip)
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(100);
            RuleFor(account => account.Take)
                .NotEmpty()
                .GreaterThanOrEqualTo(1)
                .LessThanOrEqualTo(100);
        }
    }
    
    public class ListMoneyAccountsQueryHadler : IRequestHandler<ListMoneyAccountsQuery, IEnumerable<MoneyAccountDto>>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public ListMoneyAccountsQueryHadler(IMapper mapper, DataContext dataContext)
        {
            _mapper = mapper;
            _dataContext = dataContext;
        }

        public async Task<IEnumerable<MoneyAccountDto>> Handle(ListMoneyAccountsQuery request, CancellationToken cancellationToken)
        {
            var accountData = await _dataContext.MoneyAccounts
                .Skip(request.Skip)
                .Take(request.Take)
                .Include(account => account.Profits)
                .AsNoTracking()
                .ToListAsync(cancellationToken: cancellationToken);

            return _mapper.Map<IEnumerable<MoneyAccountDto>>(accountData);
        }
    }
}