using System.Threading;
using System.Threading.Tasks;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.MoneyAccounts.Commands
{
    public class DeleteMoneyAccountCommand : IRequest<bool>
    {
        public int Id { get;}

        public DeleteMoneyAccountCommand(int id)
        {
            Id = id;
        }
    }
    
    // public class DeleteMoneyAccountCommandValidator : AbstractValidator<DeleteMoneyAccountCommand>
    // {
    //     public DeleteMoneyAccountCommandValidator()
    //     {
    //         RuleFor(account => account.Id)
    //             .NotEmpty()
    //             .GreaterThanOrEqualTo(1);
    //     }
    // }

    public class DeleteMoneyAccountCommandHadler : IRequestHandler<DeleteMoneyAccountCommand, bool>
    {
        private readonly DataContext _dataContext;

        public DeleteMoneyAccountCommandHadler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<bool> Handle(DeleteMoneyAccountCommand request, CancellationToken cancellationToken)
        {
            var moneyAccount = await _dataContext.MoneyAccounts
                .FirstOrDefaultAsync(c => c.Id == request.Id, cancellationToken: cancellationToken);
            if (moneyAccount == null)
            {
                return false;
            }
            _dataContext.MoneyAccounts.Remove(moneyAccount);
            await _dataContext.SaveChangesAsync(cancellationToken);
            
            return true;
            

        }
    }
}