using System.Threading;
using System.Threading.Tasks;
using DataAccessLayer;
using DataAccessLayer.Models;
using FluentValidation;
using MediatR;

namespace WebApi.Domain.MoneyAccounts.Commands
{
    public class CreateMoneyAccountCommand : IRequest
    {
        public string Name { get; set; }
        public string Comment { get; set; }
    }
    
    public class CreateMoneyAccountCommandValidator : AbstractValidator<CreateMoneyAccountCommand>
    {
        public CreateMoneyAccountCommandValidator()
        {
            RuleFor(account => account.Name)
                .NotEmpty()
                .MaximumLength(30);
        }
    }

    public class CreateMoneyAccountCommandHadler : IRequestHandler<CreateMoneyAccountCommand>
    {
        private readonly DataContext _dataContext;

        public CreateMoneyAccountCommandHadler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Unit> Handle(CreateMoneyAccountCommand request, CancellationToken cancellationToken)
        {
            await _dataContext.AddAsync(new MoneyAccount
            {
                Name = request.Name,
                Comment = request.Comment,
            }, cancellationToken);
            
            await _dataContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}