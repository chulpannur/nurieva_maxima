using System.Threading;
using System.Threading.Tasks;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.MoneyAccounts.Commands
{
    public class UpdateMoneyAccountCommand : IRequest<bool>
    {
        public int Id { get;}
        public string Name { get;}
        public string Comment { get; }
        
        public UpdateMoneyAccountCommand(int id, string name, string comment)
        {
            Id = id;
            Name = name;
            Comment = comment;
        }
    }
    
    public class UpdateMoneyAccountCommandValidator : AbstractValidator<UpdateMoneyAccountCommand>
    {
        public UpdateMoneyAccountCommandValidator()
        {
            RuleFor(account => account.Id)
                .NotEmpty()
                .GreaterThanOrEqualTo(1);
        }
    }

    public class UpdateExpenseCommandHadler : IRequestHandler<UpdateMoneyAccountCommand, bool>
    {
        private readonly DataContext _dataContext;

        public UpdateExpenseCommandHadler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<bool> Handle(UpdateMoneyAccountCommand request, CancellationToken cancellationToken)
        {
            var account = await _dataContext.MoneyAccounts
                .FirstOrDefaultAsync(e => e.Id == request.Id, cancellationToken: cancellationToken);
            if (account == null)
                return false;
            
            account.Name = request.Name;
            account.Comment = request.Comment;

            await _dataContext.SaveChangesAsync(cancellationToken);
            return true;

        }
    }
}