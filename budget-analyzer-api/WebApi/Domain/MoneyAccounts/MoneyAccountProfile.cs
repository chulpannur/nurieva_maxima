using AutoMapper;
using DataAccessLayer.Models;

namespace WebApi.Domain.MoneyAccounts
{
    public class MoneyAccountProfile : Profile
    {
        public MoneyAccountProfile()
        {
            CreateMap<MoneyAccountDto, MoneyAccount>();
            CreateMap<MoneyAccount, MoneyAccountDto>();
        }
    }
}