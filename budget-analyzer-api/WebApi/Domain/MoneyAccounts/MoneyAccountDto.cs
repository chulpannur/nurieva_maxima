namespace WebApi.Domain.MoneyAccounts
{
    public class MoneyAccountDto
    {
        /// <summary>
        ///
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Название счета
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Комментарии
        /// </summary>
        public string Comment { get; set; }
    }
}