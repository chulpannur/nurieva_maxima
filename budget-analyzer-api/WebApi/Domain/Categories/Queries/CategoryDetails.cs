using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.Categories.Queries
{
    public class CategoryDetails : IRequest<CategoryDto>
    {
        public int CategoryId { get; }

        public CategoryDetails(int categoryId)
        {
            CategoryId = categoryId;
        }
    }

    public class CategoryDetailsHadler : IRequestHandler<CategoryDetails, CategoryDto>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public CategoryDetailsHadler(IMapper mapper, DataContext dataContext)
        {
            _mapper = mapper;
            _dataContext = dataContext;
        }

        public async Task<CategoryDto> Handle(CategoryDetails request, CancellationToken cancellationToken)
        {
            var category = await _dataContext.Categories
                .FirstOrDefaultAsync(category => category.Id == request.CategoryId, cancellationToken: cancellationToken);
                
            return _mapper.Map<CategoryDto>(category);
        }
    }
}