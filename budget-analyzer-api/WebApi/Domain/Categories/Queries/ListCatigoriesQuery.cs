using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.Categories.Queries
{
    public class ListCategoriesQuery : IRequest<IEnumerable<CategoryDto>>
    {
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    
    public class ListCategoriesQueryValidator : AbstractValidator<ListCategoriesQuery>
    {
        public ListCategoriesQueryValidator()
        {
            RuleFor(category => category.Skip)
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(100);
            RuleFor(category => category.Take)
                .NotEmpty()
                .GreaterThanOrEqualTo(1)
                .LessThanOrEqualTo(100);
        }
    }
    
    public class ListCategoriesQueryHadler : IRequestHandler<ListCategoriesQuery, IEnumerable<CategoryDto>>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public ListCategoriesQueryHadler(IMapper mapper, DataContext dataContext)
        {
            _mapper = mapper;
            _dataContext = dataContext;
        }

        public async Task<IEnumerable<CategoryDto>> Handle(ListCategoriesQuery request, CancellationToken cancellationToken)
        {
            var categoryData = await _dataContext.Categories
                .Skip(request.Skip)
                .Take(request.Take)
                .Include(expense => expense.Expenses)
                .AsNoTracking()
                .ToListAsync(cancellationToken: cancellationToken);

            return _mapper.Map<IEnumerable<CategoryDto>>(categoryData);
        }
    }
}