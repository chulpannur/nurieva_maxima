namespace WebApi.Domain.Categories
{
    public class CategoryDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Название категория
        /// </summary>
        public string CategoryName { get; set; }
        
        /// <summary>
        /// Комментарии
        /// </summary>
        public string Comment { get; set; }
        
    }
}