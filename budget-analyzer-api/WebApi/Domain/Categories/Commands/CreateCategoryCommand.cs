using System.Threading;
using System.Threading.Tasks;
using DataAccessLayer;
using DataAccessLayer.Models;
using FluentValidation;
using MediatR;

namespace WebApi.Domain.Categories.Commands
{
    public class CreateCategoryCommand : IRequest
    {
        public string Name { get; set; }
        public string Comment { get; set; }
    }
    
    public class CreateCategoryCommandValidator : AbstractValidator<CreateCategoryCommand>
    {
        public CreateCategoryCommandValidator()
        {
            RuleFor(category => category.Name)
                .NotEmpty();
        }
    }

    public class CreateCategoryCommandHadler : IRequestHandler<CreateCategoryCommand>
    {
        private readonly DataContext _dataContext;

        public CreateCategoryCommandHadler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Unit> Handle(CreateCategoryCommand request, CancellationToken cancellationToken)
        {
            await _dataContext.AddAsync(new Category
            {
                CategoryName = request.Name,
                Comment = request.Comment,
            }, cancellationToken);
            
            await _dataContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}