using System.Threading;
using System.Threading.Tasks;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.Categories.Commands
{
    public class DeleteCategoryCommand : IRequest<bool>
    {
        public int Id { get;}

        public DeleteCategoryCommand(int id)
        {
            Id = id;
        }
    }
    
    public class DeleteCategoryCommandValidator : AbstractValidator<DeleteCategoryCommand>
    {
        public DeleteCategoryCommandValidator()
        {
            RuleFor(category => category.Id)
                .NotEmpty()
                .GreaterThanOrEqualTo(1);
        }
    }

    public class DeleteCategoryCommandHadler : IRequestHandler<DeleteCategoryCommand, bool>
    {
        private readonly DataContext _dataContext;

        public DeleteCategoryCommandHadler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<bool> Handle(DeleteCategoryCommand request, CancellationToken cancellationToken)
        {
            var category = await _dataContext.Categories
                .FirstOrDefaultAsync(c => c.Id == request.Id, cancellationToken: cancellationToken);
            if (category == null)
            {
                return false;
            }
            _dataContext.Categories.Remove(category);
            await _dataContext.SaveChangesAsync(cancellationToken);
            
            return true;
            

        }
    }
}