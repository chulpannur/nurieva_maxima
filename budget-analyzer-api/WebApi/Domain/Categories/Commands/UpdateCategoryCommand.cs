using System.Threading;
using System.Threading.Tasks;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.Categories.Commands
{
    public class UpdateCategoryCommand : IRequest<bool>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
    }

    public class UpdateCategoryCommandValidator : AbstractValidator<UpdateCategoryCommand>
    {
        public UpdateCategoryCommandValidator()
        {
            RuleFor(category => category.Id)
                .NotEmpty()
                .GreaterThanOrEqualTo(1);
            RuleFor(category => category.Name)
                .NotEmpty();
        }
    }

    public class UpdateCategoryCommandHadler : IRequestHandler<UpdateCategoryCommand, bool>
    {
        private readonly DataContext _dataContext;

        public UpdateCategoryCommandHadler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<bool> Handle(UpdateCategoryCommand request, CancellationToken cancellationToken)
        {
            var category = await _dataContext.Categories
                .FirstOrDefaultAsync(c => c.Id == request.Id, cancellationToken: cancellationToken);
            if (category == null)
                return false;

            category.CategoryName = request.Name;
            category.Comment = request.Comment;

            await _dataContext.SaveChangesAsync(cancellationToken);
            return true;

        }
    }
}