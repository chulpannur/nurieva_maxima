using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.Users.Queries
{
    public class ListUsersQuery : IRequest<IEnumerable<UserDto>>
    {
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    
    public class ListUsersQueryValidator : AbstractValidator<ListUsersQuery>
    {
        public ListUsersQueryValidator()
        {
            RuleFor(user => user.Skip)
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(100);
            RuleFor(user => user.Take)
                .NotEmpty()
                .GreaterThanOrEqualTo(1)
                .LessThanOrEqualTo(100);
        }
    }
    
    public class ListUsersQueryHadler : IRequestHandler<ListUsersQuery, IEnumerable<UserDto>>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public ListUsersQueryHadler(IMapper mapper, DataContext dataContext)
        {
            _mapper = mapper;
            _dataContext = dataContext;
        }
        
        public async Task<IEnumerable<UserDto>> Handle(ListUsersQuery request, CancellationToken cancellationToken)
        {
            var userData = await _dataContext.Users
                .Skip(request.Skip)
                .Take(request.Take)
                .Include(user => user.Profits)
                .AsNoTracking()
                .ToListAsync(cancellationToken: cancellationToken);

            return _mapper.Map<IEnumerable<UserDto>>(userData);
        }
    }
}