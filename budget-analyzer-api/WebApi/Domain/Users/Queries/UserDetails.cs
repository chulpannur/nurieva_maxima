using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.Users.Queries
{
    public class UserDetails : IRequest<UserDto>
    {
        public int Id { get; }

        public UserDetails(int id)
        {
            Id = id;
        }
    }
    
    public class UserDetailsHadler : IRequestHandler<UserDetails, UserDto>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public UserDetailsHadler(IMapper mapper, DataContext dataContext)
        {
            _mapper = mapper;
            _dataContext = dataContext;
        }

        public async Task<UserDto> Handle(UserDetails request, CancellationToken cancellationToken)
        {
            var user = await _dataContext.Users
                .Include(user=>user.Expenses)
                .Include(user=>user.Profits)
                .AsNoTracking()
                .FirstOrDefaultAsync(profit => profit.Id == request.Id, cancellationToken: cancellationToken);
                
            return _mapper.Map<UserDto>(user);
        }
    }
}