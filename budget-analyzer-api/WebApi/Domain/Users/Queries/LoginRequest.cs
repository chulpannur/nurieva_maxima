using System;
using System.Threading;
using System.Threading.Tasks;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebApi.Services;

namespace WebApi.Domain.Users.Queries
{
    public class LoginRequest : IRequest<string>
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
    
    public class LoginRequestValidator : AbstractValidator<LoginRequest>
    {
        public LoginRequestValidator()
        {
            RuleFor(user => user.Login)
                .NotEmpty();
            RuleFor(user => user.Password)
                .NotEmpty();
        }
    }
    
    public class LoginRequestHadler : IRequestHandler<LoginRequest, string>
    {
        private readonly DataContext _dataContext;
        private readonly IJwtGenerator _jwtGenerator;

        public LoginRequestHadler(DataContext dataContext, IJwtGenerator jwtGenerator)
        {
            _dataContext = dataContext;
            _jwtGenerator = jwtGenerator;
        }

        public async Task<string> Handle(LoginRequest request, CancellationToken cancellationToken)
        {
            var user = await _dataContext.Users
                    .FirstAsync(user=> user.Login == request.Login 
                                       && user.Password == request.Password, cancellationToken: cancellationToken);
                var token = _jwtGenerator.Create(user.Login, user.Role.ToString(),user.Id);

                return token;
        }
    }
}