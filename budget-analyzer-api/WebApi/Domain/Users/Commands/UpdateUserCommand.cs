using System.Threading;
using System.Threading.Tasks;
using DataAccessLayer;
using DataAccessLayer.Enums;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.Users.Commands
{
    public class UpdateUserCommand : IRequest<bool>
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Role { get; set; }

    }
    
    public class UpdateUserCommandValidator : AbstractValidator<UpdateUserCommand>
    {
        public UpdateUserCommandValidator()
        {
            RuleFor(user => user.Id)
                .NotEmpty()
                .GreaterThanOrEqualTo(1);
            RuleFor(user => user.FirstName)
                .NotEmpty().MaximumLength(30);
            RuleFor(user => user.LastName)
                .NotEmpty().MaximumLength(30);
        }
    }

    public class UpdateProfitCommandHadler : IRequestHandler<UpdateUserCommand, bool>
    {
        private readonly DataContext _dataContext;

        public UpdateProfitCommandHadler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<bool> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _dataContext.Users
                .FirstOrDefaultAsync(u => u.Id == request.Id, cancellationToken: cancellationToken);
            if (user == null)
                return false;
            
            user.FirstName = request.FirstName;
            user.LastName = request.LastName;
            user.Role = (Role)request.Role;
            
            await _dataContext.SaveChangesAsync(cancellationToken);
            return true;

        }
    }
}