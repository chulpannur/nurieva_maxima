using System.Threading;
using System.Threading.Tasks;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.Users.Commands
{
    public class DeleteUserCommand : IRequest<bool>
    {
        public int Id { get; }

        public DeleteUserCommand(int id)
        {
            Id = id;
        }
    }
    
    // public class DeleteUserCommandValidator : AbstractValidator<DeleteUserCommand>
    // {
    //     public DeleteUserCommandValidator()
    //     {
    //         RuleFor(profit => profit.Id)
    //             .NotEmpty()
    //             .GreaterThanOrEqualTo(1);
    //     }
    // }

    public class DeleteUserCommandHadler : IRequestHandler<DeleteUserCommand, bool>
    {
        private readonly DataContext _dataContext;

        public DeleteUserCommandHadler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<bool> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _dataContext.Users
                .FirstOrDefaultAsync(p => p.Id == request.Id, cancellationToken: cancellationToken);
            if (user == null)
            {
                return false;
            }
            _dataContext.Users.Remove(user);
            await _dataContext.SaveChangesAsync(cancellationToken);
            
            return true;
            

        }
    }
}