using System.Threading;
using System.Threading.Tasks;
using DataAccessLayer;
using DataAccessLayer.Enums;
using DataAccessLayer.Models;
using FluentValidation;
using MediatR;

namespace WebApi.Domain.Users.Commands
{
    public class CreateUserCommand : IRequest
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Role { get; set; }
    }
    
    public class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
    {
        public CreateUserCommandValidator()
        {
            RuleFor(user => user.Login)
                .NotEmpty()
                .MinimumLength(4)
                .MaximumLength(30);
            RuleFor(user => user.Password)
                .NotEmpty()
                .MinimumLength(8)
                .MaximumLength(30);
            RuleFor(user => user.FirstName)
                .NotEmpty()
                .MaximumLength(30);
            RuleFor(user => user.LastName)
                .NotEmpty()
                .MaximumLength(30);
            RuleFor(user => user.Role)
                .NotEmpty().GreaterThanOrEqualTo(0).LessThanOrEqualTo(4);
        }
    }

    public class CreateUserCommandHadler : IRequestHandler<CreateUserCommand>
    {
        private readonly DataContext _dataContext;

        public CreateUserCommandHadler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Unit> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            await _dataContext.AddAsync(new User
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Role = (Role)request.Role
            }, cancellationToken);

            await _dataContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}