using AutoMapper;
using DataAccessLayer.Models;

namespace WebApi.Domain.Users
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserDto, User>()
                .ForMember(user=>user.Role, e=>e
                    .MapFrom(dto=>dto.Role));
            CreateMap<User, UserDto>()
                .ForMember(user=>user.Role, e=>e
                    .MapFrom(dto=>dto.Role));
        }
    }
}