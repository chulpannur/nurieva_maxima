using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using DataAccessLayer;
using DataAccessLayer.Models;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.Expenses.Commands
{
    public class CreateExpenseCommand : IRequest
    {
        public DateTime ExpenseDateTime { get; }
        public double Amount { get; }
        public int CategoryId { get; }
        public int UserId { get; }
        
        public CreateExpenseCommand(DateTime expenseDateTime, double amount, int categoryId, int userId)
        {
            ExpenseDateTime = expenseDateTime;
            Amount = amount;
            CategoryId = categoryId;
            UserId = userId;
        }
    }
    
    public class CreateExpenseCommandValidator : AbstractValidator<CreateExpenseCommand>
    {
        public CreateExpenseCommandValidator()
        {
            RuleFor(expense => expense.ExpenseDateTime)
                .NotEmpty().NotNull();
            RuleFor(expense => expense.Amount)
                .NotEmpty().NotNull();;
            RuleFor(expense => expense.CategoryId)
                .NotEmpty().NotNull();;
        }
    }

    public class CreateExpenseCommandHadler : IRequestHandler<CreateExpenseCommand>
    {
        private readonly DataContext _dataContext;

        public CreateExpenseCommandHadler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Unit> Handle(CreateExpenseCommand request, CancellationToken cancellationToken)
        {
            await _dataContext.AddAsync(new Expense
            {
                ExpenseDateTime = request.ExpenseDateTime,
                Amount = request.Amount,
                CategoryId = request.CategoryId,
                UserId = request.UserId
            }, cancellationToken);
            
            await _dataContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;

        }
    }
}