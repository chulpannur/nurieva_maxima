using System.Threading;
using System.Threading.Tasks;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.Expenses.Commands
{
    public class DeleteExpenseCommand : IRequest<bool>
    {
        public int Id { get;}

        public DeleteExpenseCommand(int id)
        {
            Id = id;
        }
    }
    
    // public class DeleteExpenseCommandValidator : AbstractValidator<DeleteExpenseCommand>
    // {
    //     public DeleteExpenseCommandValidator()
    //     {
    //         RuleFor(expense => expense.Id)
    //             .NotEmpty()
    //             .GreaterThanOrEqualTo(1);
    //     }
    // }

    public class DeleteExpenseCommandHadler : IRequestHandler<DeleteExpenseCommand, bool>
    {
        private readonly DataContext _dataContext;

        public DeleteExpenseCommandHadler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<bool> Handle(DeleteExpenseCommand request, CancellationToken cancellationToken)
        {
            var expense = await _dataContext.Expenses
                .FirstOrDefaultAsync(c => c.Id == request.Id, cancellationToken: cancellationToken);
            if (expense == null)
            {
                return false;
            }
            _dataContext.Expenses.Remove(expense);
            await _dataContext.SaveChangesAsync(cancellationToken);
            
            return true;
            

        }
    }
}