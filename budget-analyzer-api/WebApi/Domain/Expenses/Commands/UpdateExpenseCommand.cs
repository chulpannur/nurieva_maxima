using System;
using System.Threading;
using System.Threading.Tasks;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.Expenses.Commands
{
    public class UpdateExpenseCommand : IRequest<bool>
    {
        public int Id { get;}
        public DateTime ExpenseDateTime { get; }
        public double Amount { get; }

        public UpdateExpenseCommand(int id, DateTime expenseDateTime, double amount)
        {
            Id = id;
            ExpenseDateTime = expenseDateTime;
            Amount = amount;
        }
    }
    
    public class UpdateExpenseCommandValidator : AbstractValidator<UpdateExpenseCommand>
    {
        public UpdateExpenseCommandValidator()
        {
            RuleFor(expense => expense.Id)
                .NotEmpty()
                .GreaterThanOrEqualTo(1);
        }
    }

    public class UpdateExpenseCommandHadler : IRequestHandler<UpdateExpenseCommand, bool>
    {
        private readonly DataContext _dataContext;

        public UpdateExpenseCommandHadler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<bool> Handle(UpdateExpenseCommand request, CancellationToken cancellationToken)
        {
            var expense = await _dataContext.Expenses
                .FirstOrDefaultAsync(e => e.Id == request.Id, cancellationToken: cancellationToken);
            if (expense == null)
                return false;
            
            expense.ExpenseDateTime = request.ExpenseDateTime;
            expense.Amount = request.Amount;

            await _dataContext.SaveChangesAsync(cancellationToken);
            return true;

        }
    }
}