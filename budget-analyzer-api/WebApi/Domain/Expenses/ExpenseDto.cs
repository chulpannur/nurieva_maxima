using System;
using WebApi.Domain.Categories;
using WebApi.Domain.Users;

namespace WebApi.Domain.Expenses
{
    public class ExpenseDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Сумма расхода
        /// </summary>
        public double Amount { get; set; }
        
        /// <summary>
        /// Дата расхода
        /// </summary>
        public DateTime ExpenseDateTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public UserDto User { get; set; }
        
        /// <summary>
        /// Категория
        /// </summary>
        public CategoryDto Category { get; set; }
    }
}