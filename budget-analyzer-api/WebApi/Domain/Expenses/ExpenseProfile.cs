using AutoMapper;
using DataAccessLayer.Models;

namespace WebApi.Domain.Expenses
{
    public class ExpenseProfile : Profile
    {
        public ExpenseProfile()
        {
            CreateMap<ExpenseDto, Expense>();
            CreateMap<Expense, ExpenseDto>();
        }
    }
}