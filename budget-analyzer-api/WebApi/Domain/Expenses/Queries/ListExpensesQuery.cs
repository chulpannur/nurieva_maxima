using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.Expenses.Queries
{
    public class ListExpensesQuery : IRequest<IEnumerable<ExpenseDto>>
    {
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    
    public class ListExpensesQueryValidator : AbstractValidator<ListExpensesQuery>
    {
        public ListExpensesQueryValidator()
        {
            RuleFor(expense => expense.Skip)
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(100);
            RuleFor(expense => expense.Take)
                .NotEmpty()
                .GreaterThanOrEqualTo(1)
                .LessThanOrEqualTo(100);
        }
    }
    
    public class ListExpensesQueryHadler : IRequestHandler<ListExpensesQuery, IEnumerable<ExpenseDto>>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public ListExpensesQueryHadler(IMapper mapper, DataContext dataContext)
        {
            _mapper = mapper;
            _dataContext = dataContext;
        }

        public async Task<IEnumerable<ExpenseDto>> Handle(ListExpensesQuery request, CancellationToken cancellationToken)
        {
            var expensesData = await _dataContext.Expenses
                .Skip(request.Skip)
                .Take(request.Take)
                .Include(expense => expense.Category)
                .Include(expense => expense.User)
                .AsNoTracking()
                .ToListAsync(cancellationToken: cancellationToken);
            
            return _mapper.Map<IEnumerable<ExpenseDto>>(expensesData);
        }
    }
}