using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccessLayer;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.Expenses.Queries
{
    public class ListExpensesForPeriodQuery : IRequest<IEnumerable<ExpenseDto>>
    {
        public int Skip { get; }
        public int Take { get; }
        public DateTime FromDate { get; }
        public DateTime ToDate { get; }
        public int UserId { get; }
        
        public ListExpensesForPeriodQuery(int userId, int skip, int take, DateTime fromDate, DateTime toDate)
        {
            UserId = userId;
            Skip = skip;
            Take = take;
            FromDate = fromDate;
            ToDate = toDate;
        }
    }
    
    public class ListExpensesForPeriodQueryValidator : AbstractValidator<ListExpensesForPeriodQuery>
    {
        public ListExpensesForPeriodQueryValidator()
        {
            RuleFor(expense => expense.Skip)
                .NotNull()
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(100);
            RuleFor(expense => expense.Take)
                .NotEmpty()
                .GreaterThanOrEqualTo(1)
                .LessThanOrEqualTo(100);
            RuleFor(expense => expense.FromDate)
                .NotEmpty();
            RuleFor(expense => expense.ToDate)
                .NotEmpty();
        }
    }
    
    public class ListExpensesForPeriodQueryHadler : IRequestHandler<ListExpensesForPeriodQuery, IEnumerable<ExpenseDto>>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public ListExpensesForPeriodQueryHadler(IMapper mapper, DataContext dataContext)
        {
            _mapper = mapper;
            _dataContext = dataContext;
        }

        public async Task<IEnumerable<ExpenseDto>> Handle(ListExpensesForPeriodQuery request, CancellationToken cancellationToken)
        {
            var expensesData = await _dataContext.Expenses
                .Skip(request.Skip)
                .Take(request.Take)
                .Where(expense => expense.ExpenseDateTime >= request.FromDate 
                                  && expense.ExpenseDateTime<=request.ToDate
                                  && expense.UserId==request.UserId)
                .Include(expense => expense.Category)
                .Include(expense => expense.User)
                .AsNoTracking()
                .ToListAsync(cancellationToken: cancellationToken);
            
            return _mapper.Map<IEnumerable<ExpenseDto>>(expensesData);
        }
    }
}