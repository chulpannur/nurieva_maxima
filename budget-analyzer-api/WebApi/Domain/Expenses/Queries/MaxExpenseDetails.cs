using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.Expenses.Queries
{
    public class MaxExpensesDetails : IRequest<ExpenseDto>
    {
        
    }

    public class MaxExpensesDetailsHadler : IRequestHandler<MaxExpensesDetails, ExpenseDto>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public MaxExpensesDetailsHadler(IMapper mapper, DataContext dataContext)
        {
            _mapper = mapper;
            _dataContext = dataContext;
        }

        public async Task<ExpenseDto> Handle(MaxExpensesDetails request, CancellationToken cancellationToken)
        {
            try
            {
                var expensesData = await _dataContext.Expenses
                    .Include(expense => expense.Category)
                    .Include(expense => expense.User)
                    .OrderByDescending(expense => expense.Amount)
                    .FirstOrDefaultAsync(cancellationToken: cancellationToken);
                
                return _mapper.Map<ExpenseDto>(expensesData);
            }
            catch (Exception e)
            {
                // ignored
            }

            return null;
        }
    }
}