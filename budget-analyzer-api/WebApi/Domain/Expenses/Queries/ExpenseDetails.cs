using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Domain.Expenses.Queries
{
    public class ExpenseDetails : IRequest<ExpenseDto>
    {
        public int ExpenseId { get; }

        public ExpenseDetails(int expenseId)
        {
            ExpenseId = expenseId;
        }
    }
    
    public class ExpenseDetailsHadler : IRequestHandler<ExpenseDetails, ExpenseDto>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public ExpenseDetailsHadler(IMapper mapper, DataContext dataContext)
        {
            _mapper = mapper;
            _dataContext = dataContext;
        }

        public async Task<ExpenseDto> Handle(ExpenseDetails request, CancellationToken cancellationToken)
        {
            var expense = await _dataContext.Expenses
                .Include(expense => expense.Category)
                .Include(expense => expense.User)
                .FirstOrDefaultAsync(expense => expense.Id == request.ExpenseId, cancellationToken: cancellationToken);
                
            return _mapper.Map<ExpenseDto>(expense);
        }
    }
}