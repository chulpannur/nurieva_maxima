using Microsoft.AspNetCore.Builder;
using WebApi.Middlewares;

namespace WebApi
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseExceptionHandler(this IApplicationBuilder applicationBuilder)
        {
            applicationBuilder.UseMiddleware<ExceptionHandlingMiddleware>();
            return applicationBuilder;
        }
    }
}