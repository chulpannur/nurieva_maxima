﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApi.Domain.MoneyAccounts;
using WebApi.Domain.MoneyAccounts.Commands;
using WebApi.Domain.MoneyAccounts.Queries;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class MoneyAccountsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public MoneyAccountsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Получение money account по номеру
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200"> Счет найден </response>
        /// <response code="404"> Счет не найден </response>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(MoneyAccountDto), 200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult> Get(int id)
        {
            if (ModelState.IsValid)
            {
                var account = new MoneyAccountDetails(id);
                var result = await _mediator.Send(account);
                if (result == null)
                    return NotFound();
                
                return Ok(result);
            }

            return BadRequest(ModelState);
        }

        /// <summary>
        /// Получение money account по номеру
        /// </summary>
        /// <response code="200"> Счета найдены </response>
        /// <response code="404"> Счета не найдены </response>
        /// <returns></returns>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(MoneyAccountDto), 200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult> GetAll([FromQuery]ListMoneyAccountsQuery moneyAccountsQuery)
        {
            var result = await _mediator.Send(moneyAccountsQuery);
            if (result == null)
                return NotFound();
            
            return Ok(result);
        }
        
        /// <summary>
        /// Изменение счета
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="comment"></param>
        /// <response code="200"> Счет изменен  </response>
        /// <response code="404"> Счет не найден </response>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult> Update(int id, string name, string comment)
        {
            if (ModelState.IsValid)
            {
                var updateMoneyAccountCommand = new UpdateMoneyAccountCommand(id, name, comment);
                var moneyAccountFound = await _mediator.Send(updateMoneyAccountCommand);
                if (!moneyAccountFound)
                    return NotFound("Money account not found");
                
                return Ok("Data is changed");
            }
        
            return BadRequest(ModelState);
        }
        
        /// <summary>
        /// Создание счета
        /// </summary>
        /// <param name="createMoneyAccountCommand"></param>
        /// <response code="200"> Счет создан  </response>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(200)]
        public async Task<ActionResult> Create([FromQuery]CreateMoneyAccountCommand createMoneyAccountCommand)
        {
            await _mediator.Send(createMoneyAccountCommand);
            return Ok();
        }
        
        /// <summary>
        /// Удаление счета
        /// </summary>
        /// <param name="id"></param>
        /// <response code="204"> Счет удален  </response>
        /// <response code="404"> Счет не найден </response>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<ActionResult> Delete(int id)
        {
            var deleteMoneyAccountCommand = new DeleteMoneyAccountCommand(id);
            var moneyAccountFound = await _mediator.Send(deleteMoneyAccountCommand);
            if (!moneyAccountFound)
                return NotFound("Money account not found");
            
            return NoContent();
        }
    }
}