﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Domain.Categories;
using WebApi.Domain.Categories.Queries;
using WebApi.Domain.Categories.Commands;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class CategoriesController : ControllerBase
    {
        private readonly  IMediator _mediator;

        public CategoriesController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        /// <summary>
        /// Получение category по номеру
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200"> Категория найдена </response>
        /// <response code="401"> user не авторизован </response>
        /// <response code="404"> Категория не найдена </response>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Get(int id)
        {
            if (ModelState.IsValid)
            {
                var category = new CategoryDetails(id);
                var result = await _mediator.Send(category);
                if (result == null)
                    return NotFound("Category not found");
                
                return Ok(result);
            }

            return BadRequest(ModelState);
        }

        /// <summary>
        /// Получение categories
        /// </summary>
        /// <param name="categoriesQuery"></param>
        /// <response code="200"> Категории найдены </response>
        /// <response code="401"> user не авторизован </response>
        /// <response code="404"> Категории не найдены </response>
        /// <returns></returns>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(CategoryDto),StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetAll([FromQuery]ListCategoriesQuery categoriesQuery)
        {
            var result = await _mediator.Send(categoriesQuery);
            if (result == null)
                return NotFound("No categories");
            
            return Ok(result);
        }
        
        /// <summary>
        /// Изменение category
        /// </summary>
        /// <param name="updateCategoryCommand"></param>
        /// <response code="200"> Категория изменена  </response>
        /// <response code="401"> user не авторизован </response>
        /// <response code="403"> нет прав </response>
        /// <response code="404"> Категория не найдена </response>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Update([FromQuery]UpdateCategoryCommand updateCategoryCommand)
        {
            var categoryFound = await _mediator.Send(updateCategoryCommand);
                if (!categoryFound)
                    return NotFound("Category not found");
                
                return Ok("Information is changed");
        }
        
        /// <summary>
        /// Создание category
        /// </summary>
        /// <param name="createCategoryCommand"></param>
        /// <response code="200"> Категория создана  </response>
        /// <response code="401"> user не авторизован </response>
        /// <response code="403"> нет прав </response>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult> Create([FromQuery]CreateCategoryCommand createCategoryCommand)
        {
            await _mediator.Send(createCategoryCommand);
            return Ok();
        }
        
        /// <summary>
        /// Удаление category
        /// </summary>
        /// <param name="id"></param>
        /// <response code="204"> Категория удалена  </response>
        /// <response code="401"> user не авторизован </response>
        /// <response code="403"> нет прав </response>
        /// <response code="404"> Категория не найдена </response>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Delete(int id)
        {
            var category = new DeleteCategoryCommand(id);
            var categoryFound = await _mediator.Send(category);
            if (!categoryFound)
                return NotFound("Category not found");
            
            return NoContent();
        }
    }
}