﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Domain.Profits;
using WebApi.Domain.Profits.Commands;
using WebApi.Domain.Profits.Queries;

namespace WebApi.Controllers
{
    [Authorize (Roles = "User, Admin")]
    [ApiController]
    [Route("api/[controller]")]
    public class ProfitsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ProfitsController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        /// <summary>
        /// Получение доходов за период
        /// </summary>
        /// <param name="listProfitsForPeriodQuery"></param>
        /// <response code="200"> Доходы найдены </response>
        /// <response code="401"> User не авторизован </response>
        /// <response code="403"> Нет прав </response>
        /// <response code="404"> Доходы не найдены </response>
        /// <returns></returns>
        [HttpGet("/for period")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ProfitDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetProfitsForPeriod([FromQuery]ListProfitsForPeriodQuery listProfitsForPeriodQuery)
        {
            var result = await _mediator.Send(listProfitsForPeriodQuery);
            if (result == null)
                return NotFound("No data");
        
            return Ok(result);
        }
        
        /// <summary>
        /// Получение дохода по номеру
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200"> Доход найден </response>
        /// <response code="401"> User не авторизован </response>
        /// <response code="403"> Нет прав </response>
        /// <response code="404"> Доход не найден </response>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ProfitDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Get(int id)
        {
            var profitDetails = new ProfitDetails(id);
            var result = await _mediator.Send(profitDetails);
            if (result == null)
                return NotFound();
        
            return Ok(result);
        }
        
        /// <summary>
        /// Получение дохода по номеру
        /// </summary>
        /// <response code="200"> Доходы найдены </response>
        /// <response code="401"> User не авторизован </response>
        /// <response code="403"> Нет прав </response>
        /// <response code="404"> Доходы не найдены </response>
        /// <returns></returns>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ProfitDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetAll([FromQuery]ListProfitsQuery profitsQuery)
        {
            var result = await _mediator.Send(profitsQuery);
            if (result == null)
                return NotFound();
        
            return Ok(result);
        }

        /// <summary>
        /// Изменение дохода
        /// </summary>
        /// <param name="updateProfitCommand"></param>
        /// <response code="200"> Доход изменен  </response>
        /// <response code="404"> Доход не найден </response>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Update([FromQuery]UpdateProfitCommand updateProfitCommand)
        {
            var profitFound = await _mediator.Send(updateProfitCommand);
                if (!profitFound)
                    return NotFound("Profit not found");
                
                return Ok("Data is changed");
        }

        /// <summary>
        /// Создание дохода
        /// </summary>
        /// <param name="profitDateTime"></param>
        /// <param name="amount"></param>
        /// <param name="moneyAccountId"></param>
        /// <response code="200"> Доход создан  </response>
        /// <response code="401"> User не авторизован </response>
        /// <response code="403"> Нет прав </response>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult> Create(DateTime profitDateTime, double amount, int moneyAccountId)
        {
            var userId = Convert.ToInt32(HttpContext.User.Claims?
                .FirstOrDefault(x => x.Type == "UserId")?
                .Value);
            var createProfitCommand = new CreateProfitCommand(profitDateTime,amount,moneyAccountId, userId);
            await _mediator.Send(createProfitCommand);
            return Ok();
        }
        
        /// <summary>
        /// Удаление дохода
        /// </summary>
        /// <param name="id"></param>
        /// <response code="204"> Доход удален  </response>
        /// <response code="401"> User не авторизован </response>
        /// <response code="403"> Нет прав </response>
        /// <response code="404"> Доход не найден </response>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Delete(int id)
        {
            var profitCommand = new DeleteProfitCommand(id);
            var profitFound = await _mediator.Send(profitCommand);
            if (!profitFound)
                return NotFound("Profit not found");
            
            return NoContent();
        }
    }
}