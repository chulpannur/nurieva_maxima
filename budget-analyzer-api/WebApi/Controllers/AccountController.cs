﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApi.Domain.Users.Queries;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IMediator _mediator;
        public AccountController(IMediator mediator)
        {
            _mediator = mediator;
        }
        /// <summary>
        /// Login
        /// </summary>
        /// <param name="loginRequest"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("/login")]
        public async Task<ActionResult<string>> Login([FromQuery] LoginRequest loginRequest)
        {
            try
            {
                var jwt = await _mediator.Send(loginRequest);
                return Ok(jwt);
            }
            catch (Exception e)
            {
                return new BadRequestResult();
            }
        }
    }
}