﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Domain.Users;
using WebApi.Domain.Users.Commands;
using WebApi.Domain.Users.Queries;

namespace WebApi.Controllers
{
    [Authorize(Policy = Policies.RequireAdminPolicy)]
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IMediator _mediator;

        public UsersController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        /// <summary>
        /// Получение user по номеру
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200"> user найден </response>
        /// <response code="401"> user не авторизован </response>
        /// <response code="403"> нет прав </response>
        /// <response code="404"> user не найден </response>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(UserDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Get(int id)
        {
            var userDetails = new UserDetails(id);
                var result = await _mediator.Send(userDetails);
                if (result == null)
                    return NotFound();
                
                return Ok(result);
        }
        
        /// <summary>
        /// Получение users 
        /// </summary>
        /// <param name="usersQuery"></param>
        /// <response code="200"> users найдены </response>
        /// <response code="404"> users не найдены </response>
        /// <response code="401"> user не авторизован </response>
        /// <response code="403"> нет прав </response>
        /// <returns></returns>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(UserDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetAll([FromQuery]ListUsersQuery usersQuery)
        {
            var result = await _mediator.Send(usersQuery);
            if (result == null)
                return NotFound("No users");
            return Ok(result);
        }
        
        /// <summary>
        /// Изменение user
        /// </summary>
        /// <param name="updateCategoryCommand"></param>
        /// <response code="200"> user изменен  </response>
        /// <response code="401"> user не авторизован </response>
        /// <response code="403"> нет прав </response>
        /// <response code="404"> user не найден </response>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Update([FromQuery]UpdateUserCommand updateCategoryCommand)
        {
            var categoryFound = await _mediator.Send(updateCategoryCommand);
            if (!categoryFound)
                return NotFound("User not found");
                
            return Ok("Information is changed");
        }
        
        /// <summary>
        /// Создание user
        /// </summary>
        /// <param name="createUserCommand"></param>
        /// <response code="200"> user создан  </response>
        /// <response code="401"> user не авторизован </response>
        /// <response code="403"> нет прав </response>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult> Create([FromQuery]CreateUserCommand createUserCommand)
        {
            if (ModelState.IsValid)
            {
                await _mediator.Send(createUserCommand);
                return Ok();
            }
        
            return BadRequest(ModelState);
        }
        
        /// <summary>
        /// Удаление user
        /// </summary>
        /// <param name="id"></param>
        /// <response code="204"> user удален  </response>
        /// <response code="401"> user не авторизован </response>
        /// <response code="403"> нет прав </response>
        /// <response code="404"> user не найден </response>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult> Delete(int id)
        {
            var deleteUserCommand = new DeleteUserCommand(id);
            var categoryFound = await _mediator.Send(deleteUserCommand);
            if (!categoryFound)
                return NotFound("User is not found");
            
            return Ok("User was deleted");
        }
    }
}