﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Domain.Expenses;
using WebApi.Domain.Expenses.Commands;
using WebApi.Domain.Expenses.Queries;

namespace WebApi.Controllers
{
    [Authorize (Roles = "User, Admin")]
    [ApiController]
    [Route("api/[controller]")]
    public class ExpensesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ExpensesController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        /// <summary>
        /// Получение max expense
        /// </summary>
        /// <response code="200"> Расход найден </response>
        /// <response code="401"> User не авторизован </response>
        /// <response code="403"> Нет прав </response>
        /// <response code="404"> Расход не найден </response>
        /// <returns></returns>
        [HttpGet("/max")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ExpenseDto),StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetMaxExpense()
        {
            var maxExpensesDetails = new MaxExpensesDetails();
            var result = await _mediator.Send(maxExpensesDetails);
            if (result == null)
                return NotFound("No data");
        
            return Ok(result);
        }

        /// <summary>
        /// Получение расходов за период
        /// </summary>
        /// <response code="200"> Расходы найдены </response>
        /// <response code="401"> User не авторизован </response>
        /// <response code="403"> Нет прав </response>
        /// <response code="404"> Расходы не найдены </response>
        /// <returns></returns>
        [HttpGet("/forperiod")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ExpenseDto),StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetExpensesForPeriod(int skip, int take, DateTime fromDate, DateTime toDate)
        {
            var userId = Convert.ToInt32(HttpContext.User.Claims?
                .FirstOrDefault(x => x.Type == "UserId")?
                .Value);
            var listExpensesForPeriodQuery = new ListExpensesForPeriodQuery(userId, skip, take, fromDate, toDate);
            var result = await _mediator.Send(listExpensesForPeriodQuery);
            if (result == null)
                return NotFound("No data");
        
            return Ok(result);
        }

        /// <summary>
        /// Получение expense по номеру
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200"> Расход найден </response>
        /// <response code="401"> User не авторизован </response>
        /// <response code="403"> Нет прав </response>
        /// <response code="404"> Расход не найден </response>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ExpenseDto),StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Get(int id)
        {
            var expense = new ExpenseDetails(id);
            var result = await _mediator.Send(expense);
            if (result == null)
                return NotFound();
        
            return Ok(result);
        }
        
        /// <summary>
        /// Получение expenses
        /// </summary>
        /// <param name="expensesQuery"></param>
        /// <response code="200"> Расходы найдены </response>
        /// <response code="401"> User не авторизован </response>
        /// <response code="403"> Нет прав </response>
        /// <response code="404"> Расходы не найдены </response>
        /// <returns></returns>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ExpenseDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetAll([FromQuery]ListExpensesQuery expensesQuery)
        {
            var result = await _mediator.Send(expensesQuery);
            if (result == null)
                return NotFound();
            
            return Ok(result);
        }
        
         /// <summary>
         /// Изменение expense
         /// </summary>
         /// <param name="id"></param>
         /// <param name="expenseDate"></param>
         /// <param name="amount"></param>
         /// <response code="200"> Расход изменен  </response>
         /// <response code="401"> User не авторизован </response>
         /// <response code="403"> Нет прав </response>
         /// <response code="404"> Расход не найден </response>
         /// <returns></returns>
         [HttpPut("{id}")]
         [ProducesResponseType(StatusCodes.Status200OK)]
         [ProducesResponseType(StatusCodes.Status401Unauthorized)]
         [ProducesResponseType(StatusCodes.Status403Forbidden)]
         [ProducesResponseType(StatusCodes.Status404NotFound)]
         public async Task<ActionResult> Update(int id, DateTime expenseDate, double amount)
        {
            var updateExpenseCommand = new UpdateExpenseCommand(id, expenseDate, amount);
                var expenseFound = await _mediator.Send(updateExpenseCommand);
                if (!expenseFound)
                    return NotFound("Expense not found");
                
                return Ok("Data is changed");
        }

         /// <summary>
         /// Создание expense
         /// </summary>
         /// <param name="expenseDateTime"></param>
         /// <param name="amount"></param>
         /// <param name="categoryId"></param>
         /// <response code="200"> Расход создан  </response>
         /// <response code="401"> User не авторизован </response>
         /// <response code="403"> Нет прав </response>
         /// <returns></returns>
         [HttpPost]
         [ProducesResponseType(StatusCodes.Status200OK)]
         [ProducesResponseType(StatusCodes.Status401Unauthorized)]
         [ProducesResponseType(StatusCodes.Status403Forbidden)]
         public async Task<ActionResult> Create([FromQuery] DateTime expenseDateTime, double amount, int categoryId)
         {
            var userId = Convert.ToInt32(HttpContext.User.Claims?
                .FirstOrDefault(x => x.Type == "UserId")?
                .Value);
            var createExpenseCommand = new CreateExpenseCommand(expenseDateTime, amount, categoryId, userId);
            await _mediator.Send(createExpenseCommand);
                return Ok();
        }
        
        /// <summary>
        /// Удаление expense
        /// </summary>
        /// <param name="id"></param>
        /// <response code="204"> Расход удален  </response>
        /// <response code="401"> User не авторизован </response>
        /// <response code="403"> Нет прав </response>
        /// <response code="404"> Расход не найден </response>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Delete(int id)
        {
            var expenseCommand = new DeleteExpenseCommand(id);
            var expenseFound = await _mediator.Send(expenseCommand);
            if (!expenseFound)
                return NotFound("Expense not found");
            
            return NoContent();
        }
    }
}