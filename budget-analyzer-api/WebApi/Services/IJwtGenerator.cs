namespace WebApi.Services
{
    public interface IJwtGenerator
    {
       public string Create(string login, string role, int userId);
    }
}