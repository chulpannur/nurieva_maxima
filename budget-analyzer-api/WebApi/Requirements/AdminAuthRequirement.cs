using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Requirements
{
    public class AdminAuthRequirement: IAuthorizationRequirement
    {
        public string AdminRole { get;}
        
            public AdminAuthRequirement(string adminRole)
            {
                AdminRole = adminRole;
            }
        

        public class AdminAuthRequirementHandler : AuthorizationHandler<AdminAuthRequirement>
        {
            protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AdminAuthRequirement requirement)
            {
                var claim = context.User.Claims.FirstOrDefault(claim1 => claim1.Type == ClaimsIdentity.DefaultRoleClaimType);
                if (claim != null)
                {
                    if (claim.Value == requirement.AdminRole)
                        context.Succeed(requirement);
                }

                return Task.CompletedTask;
            }
        }

    }
}