namespace WebApi
{
    public class AppOptions
    {
        /// <summary>
        /// Default connection
        /// </summary>
        public string DbConnection { get; set; }
        
        /// <summary>
        /// Secret key
        /// </summary>
        public string Key { get; set; }
    }
}