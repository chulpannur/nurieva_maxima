namespace DataAccessLayer.Enums
{
    public enum Role
    {
        Disable,
        Guest,
        User,
        Admin
    }
}