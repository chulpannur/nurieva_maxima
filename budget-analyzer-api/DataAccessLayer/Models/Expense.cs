using System;

namespace DataAccessLayer.Models
{
    public class Expense
    {
        public int Id { get; set; }
        public double Amount { get; set; }
        public DateTime ExpenseDateTime { get; set; }
        public int UserId { get; set; }
        public int CategoryId { get; set; }
        public virtual User User { get; set; }
        public virtual Category Category { get; set; }
    }
}