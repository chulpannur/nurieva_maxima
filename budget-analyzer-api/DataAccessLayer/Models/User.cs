using System.Collections.Generic;
using DataAccessLayer.Enums;

namespace DataAccessLayer.Models
{
    
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Role Role { get; set; }
        public ICollection<Expense> Expenses { get; set; }
        public ICollection<Profit> Profits { get; set; }
        
        
        public string Login { get; set; }
        
        public string Password { get; set; }
    }
}