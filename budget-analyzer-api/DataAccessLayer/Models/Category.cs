using System.Collections.Generic;

namespace DataAccessLayer.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        
        public string Comment { get; set; }
        
        public ICollection<Expense> Expenses { get; set; }
        
    }
}