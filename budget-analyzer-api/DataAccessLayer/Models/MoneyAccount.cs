using System.Collections.Generic;

namespace DataAccessLayer.Models
{
    public class MoneyAccount
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public ICollection<Profit> Profits { get; set; }
    }
}