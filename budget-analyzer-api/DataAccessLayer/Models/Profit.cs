using System;

namespace DataAccessLayer.Models
{
    public class Profit
    {
        public int Id { get; set; }
        public DateTime ProfitDateTime { get; set; }
        public double Amount { get; set; }
        public int MoneyAccountId { get; set; }
        public int UserId { get; set; }
        public virtual MoneyAccount MoneyAccount { get; set; }
        public virtual User User { get; set; }
    }
}