using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace DataAccessLayer
{
    public class DesignTimeDbDataContextFactory: IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var optionsBuilder=new DbContextOptionsBuilder<DataContext>()
                .UseNpgsql("Host=localhost;Port=5432;Username=postgres;Password=1234;Database=BudgetAnalyzerDB");
            return new DataContext(optionsBuilder.Options);
        }
    }
}