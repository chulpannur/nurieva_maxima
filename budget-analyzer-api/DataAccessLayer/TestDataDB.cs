using System;
using System.Collections.Generic;
using DataAccessLayer.Enums;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer
{
    public static class TestDataDB
    {
        public static void FillDataDB(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Id = 1, 
                    FirstName = "Chulpan", 
                    LastName = "Nurieva", 
                    Role = Role.User, 
                    Login = "user1",
                    Password = "password1"
                },
                new User
                {
                    Id = 2, 
                    FirstName = "Ivan", 
                    LastName = "Ivanov", 
                    Role = Role.User, 
                    Login = "user2",
                    Password = "password2"
                },
                new User
                {
                    Id = 3, 
                    FirstName = "Ivan", 
                    LastName = "Sidorov", 
                    Role = Role.Guest, 
                    Login = "user3",
                    Password = "password3"
                },
                new User
                {
                    Id = 4, 
                    FirstName = "Ivan", 
                    LastName = "Sidorov2", 
                    Role = Role.Admin, 
                    Login = "user4",
                    Password = "password4"
                }
            );
            modelBuilder.Entity<MoneyAccount>().HasData(
                new MoneyAccount {Id = 1, Name = "Счет1", Comment = "проект1"},
                new MoneyAccount {Id = 2, Name = "Счет2", Comment = "..."},
                new MoneyAccount {Id = 3, Name = "Счет3", Comment = "..."}
            );
            modelBuilder.Entity<Category>().HasData(
                new Category {Id = 1, CategoryName = "Коммунальные платежи", Comment = "свет, вода и т.д."},
                new Category {Id = 2, CategoryName = "Развлечение", Comment = "корпоративы, караоке и т.д."},
                new Category {Id = 3, CategoryName = "Обучение", Comment = "англ, курсы и т.д."}
            );
            modelBuilder.Entity<Profit>().HasData(
                new Profit
                {
                    Id = 1, 
                    ProfitDateTime = Convert.ToDateTime("2022/01/10"), 
                    Amount = 30000, 
                    MoneyAccountId = 1,
                    UserId = 1
                },
                new Profit
                {
                    Id = 2, 
                    ProfitDateTime = Convert.ToDateTime("2022/01/11"), 
                    Amount = 120000, 
                    MoneyAccountId = 2,
                    UserId = 2
                },
                new Profit
                {
                    Id = 3, 
                    ProfitDateTime = Convert.ToDateTime("2022/01/12"), 
                    Amount = 320000, 
                    MoneyAccountId = 1,
                    UserId = 1
                },
                new Profit
                {
                    Id = 4, 
                    ProfitDateTime = Convert.ToDateTime("2022/02/12"), 
                    Amount = 300000, 
                    MoneyAccountId = 1,
                    UserId = 2
                }
            );
            modelBuilder.Entity<Expense>().HasData(
                new Expense
                {
                    Id = 1, 
                    ExpenseDateTime = Convert.ToDateTime("2022/02/10"), 
                    Amount = 30000, 
                    CategoryId = 1,
                    UserId = 1
                },
                new Expense
                {
                    Id = 2, 
                    ExpenseDateTime = Convert.ToDateTime("2022/01/11"), 
                    Amount = 10000, 
                    CategoryId = 2,
                    UserId = 2
                },
                new Expense
                {
                    Id = 3, 
                    ExpenseDateTime = Convert.ToDateTime("2022/02/02"), 
                    Amount = 32000, 
                    CategoryId = 1,
                    UserId = 1
                },
                new Expense
                {
                    Id = 4, 
                    ExpenseDateTime = Convert.ToDateTime("2022/02/12"), 
                    Amount = 60000, 
                    CategoryId = 3,
                    UserId = 2
                }
            );
        }
    }
}