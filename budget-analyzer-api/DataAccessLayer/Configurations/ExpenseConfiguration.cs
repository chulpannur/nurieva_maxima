using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations
{
    public class ExpenseConfiguration : IEntityTypeConfiguration<Expense>
    {
        public void Configure(EntityTypeBuilder<Expense> builder)
        {
            builder.Property(p => p.Amount)
                .IsRequired();
            builder.Property(p => p.ExpenseDateTime)
                .IsRequired();
            builder
                .HasOne(expense => expense.User)
                .WithMany(user => user.Expenses);
            builder
                .HasOne(expense => expense.Category)
                .WithMany(user => user.Expenses);
        }
    }
}