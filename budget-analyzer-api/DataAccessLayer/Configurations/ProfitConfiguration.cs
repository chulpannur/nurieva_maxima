using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations
{
    public class ProfitConfiguration : IEntityTypeConfiguration<Profit>
    {
        public void Configure(EntityTypeBuilder<Profit> builder)
        {
            builder.Property(p => p.Amount)
                .IsRequired();
            builder.Property(p => p.ProfitDateTime)
                .IsRequired();
            builder
                .HasOne(profit => profit.User)
                .WithMany(user => user.Profits);
            builder
                .HasOne(profit => profit.MoneyAccount)
                .WithMany(account => account.Profits);
        }
    }
}