using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations
{
    public class MoneyAccountConfiguration : IEntityTypeConfiguration<MoneyAccount>
    {
        public void Configure(EntityTypeBuilder<MoneyAccount> builder)
        {
            builder.Property(p => p.Name)
                .IsRequired();
        }
    }
}