using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(p => p.Login)
                .IsRequired()
                .HasMaxLength(30);
            builder.Property(p => p.Password)
                .IsRequired()
                .HasMaxLength(30);
            builder.Property(p => p.FirstName)
                .IsRequired()
                .HasMaxLength(30);
            builder.Property(p => p.LastName)
                .IsRequired()
                .HasMaxLength(30);
            builder.Property(p => p.Role)
                .IsRequired();
        }
    }
}