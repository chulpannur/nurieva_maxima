﻿using Microsoft.AspNetCore.Builder;
using WebApi.Middlewares;

namespace WebApi
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseExceptionHandler(this IApplicationBuilder applicationBuilder)
        {
            applicationBuilder.UseMiddleware<ExceptionMiddleware>();
            return applicationBuilder;
        }
        
        public static IApplicationBuilder UseTokenAuth(this IApplicationBuilder applicationBuilder, string token)
        {
            applicationBuilder.UseMiddleware<TokenMiddleware>(token);
            return applicationBuilder;
        }
        
        public static IApplicationBuilder UseLogUrl(this IApplicationBuilder applicationBuilder)
        {
            applicationBuilder.UseMiddleware<LogUrlMiddleware>();
            return applicationBuilder;
        }
    }
}