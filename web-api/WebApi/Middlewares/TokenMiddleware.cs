﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace WebApi.Middlewares
{
    public class TokenMiddleware
    {
        private readonly RequestDelegate _next;

        private readonly string _token;
        
        public TokenMiddleware(RequestDelegate next, string token)
        {
            _next = next;
            _token = token;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (context.Request.Query["token"] == _token)
            {
                await _next(context);
            }
            else 
            {
                context.Response.StatusCode = StatusCodes.Status403Forbidden;
                await context.Response.WriteAsync("Token is invalid");
            }
        }
    }
}