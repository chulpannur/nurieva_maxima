﻿namespace WebApi
{
    public enum Role
    {
        Disable,
        Guest,
        User,
        Admin
    }
}