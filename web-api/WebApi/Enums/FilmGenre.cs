﻿using System;

namespace WebApi
{
    public enum FilmGenre : byte
    {
        Action,
        Comedy,
        Documentary,
        Drama,
        Horror,
        Adventure,
        Thriller,
        Musical,
        Detective
    }
}