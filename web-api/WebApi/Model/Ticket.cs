using System;

namespace WebApi.Model
{
    public class Ticket
    {
        public int Id { get; set; }
        public string Title { get; set; }
        
        public string FilmGenre { get; set; }

        public int OrderNumber { get; set; }

        public DateTime FilmTime { get; set; }
        
        public double Cost { get; set; }
        
        public int RoomNumber { get; set; }
        
        public int Row { get; set; }
        
        public int Seat { get; set; }
    }
}