﻿using System.Collections.Generic;

namespace WebApi.Model
{
    public class User
    {
        public int Id { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public List<Role> Roles { get; set; }
    }
}