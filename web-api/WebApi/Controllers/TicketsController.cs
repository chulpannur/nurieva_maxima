﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApi.Model;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TicketsController : ControllerBase
    {
        private static readonly List<Ticket> Tickets = new();
        
        private readonly ILogger<TicketsController> _logger;

        public TicketsController(ILogger<TicketsController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public List<Ticket> GetAll(int skip, int take)
        {
            return Tickets.Skip(skip).Take(take).ToList();
        }
        
        [HttpGet("{id}")]
        public Ticket GetById(int id)
        {
            return Tickets.Find(ticket => ticket.Id == id);
        }
        
        [HttpPut]
        public void Edit(Ticket ticket)
        {
            var item=Tickets.First(_=>_.Id==ticket.Id);
            item.Title = ticket.Title;
            item.Seat = ticket.Seat;
        }
        
        [HttpPost]
        public void Create(Ticket ticket)
        {
            Tickets.Add(ticket);
        }
        
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var item = Tickets.Find(item => item.Id == id);
            Tickets.Remove(item);
        }
    }
}