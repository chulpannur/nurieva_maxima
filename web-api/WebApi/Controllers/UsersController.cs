﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebApi.Model;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private static readonly List<User> Users = new List<User>()
        {
            new User()
            {
                Id = 0,
                FirstName = "FirstName-0",
                LastName = "LastName-0",
                Roles = new List<Role>()
                {
                    Role.Admin,
                    Role.User
                }
            },
            new User()
            {
                Id   = 1,
                FirstName = "FirstName-1",
                LastName = "LastName-1",
                Roles = new List<Role>()
                {
                    Role.User,
                    Role.Guest
                }
            }
        };

        [HttpGet]
        public List<User> GetAll()
        {
            return Users;
        }
        
        [HttpGet("{id}")]
        public User GetById(int id)
        {
            return Users.Find(user => user.Id == id);
        }
        
        [HttpPut]
        public void Edit(User user)
        {
            var item=Users.First(_=>_.Id==user.Id);
            item.FirstName = user.FirstName;
            item.LastName = user.LastName;
            item.Roles = user.Roles;
        }
        
        [HttpPost]
        public void Create(User user)
        {
            Users.Add(user);
        }
        
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var item = Users.Find(item => item.Id == id);
            Users.Remove(item);
        }
    }
}