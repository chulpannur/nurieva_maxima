namespace Homework_10
{
    public interface IFigure
    {
        double GetArea();
    }
}