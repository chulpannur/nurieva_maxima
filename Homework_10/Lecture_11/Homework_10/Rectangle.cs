namespace Homework_10
{
    public class Rectangle: IFigure, INamable
    {
        private string name;
        
        private double a;

        private double b;
        
        public string Name => name;

        public Rectangle(string name, double a, double b)
        {
            this.name = name;
            this.a = a;
            this.b = b;
        }
        
        public double GetArea()
        {
            return a * b;
        }
        
    }
}