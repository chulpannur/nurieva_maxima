﻿using System;

namespace Homework_10
{
    class Program
    {
        static void Main(string[] args)
        {
            var rectangle = new Rectangle("rectangle", 4, 5);
            
            INamable namable = rectangle;
            IFigure figure = rectangle;
            
            Console.WriteLine(namable.Name);
            Console.WriteLine(figure.GetArea());
            
        }
    }
}