namespace Homework_10
{
    public interface INamable
    {
        public string Name { get; }
    }
}