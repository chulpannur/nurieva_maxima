﻿using System;
using SortClassLibrary;

namespace Homework_14
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            int[] intArray = new int[10];

            for (int i = 0; i < intArray.Length; i++)
            {
                intArray[i] = random.Next(10,20);
            }
            intArray.PrintArray();
            
            Console.WriteLine("Sort:");
            intArray.PuzirokSort();
            intArray.PrintArray();
        }
    }
}