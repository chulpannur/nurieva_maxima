﻿using System;

namespace SortClassLibrary
{
    public static class ArrayExtensions
    {
        public static void PuzirokSort(this int[] intArray)
        {
            int temp;
            for (int i = 0; i < intArray.Length ; i++)
            {
                for (int j = i + 1; j < intArray.Length; j++)
                {
                    if (intArray[i] > intArray[j])
                    {
                        temp = intArray[i];
                        intArray[i] = intArray[j];
                        intArray[j] = temp;
                    }
                }
            }
        }

        public static void PrintArray(this int[] intArray)
        {
            foreach (var i in intArray)
            {
                Console.WriteLine(i);
            }
        }
    }
}