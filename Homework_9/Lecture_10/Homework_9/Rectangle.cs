using System;

namespace Homework_9
{
    public class Rectangle: Figure
    {
        public override double Area => a * b;

        private double a;

        private double b;

        public Rectangle(string name, double a, double b) : base(name)
        {
            this.a = a;
            this.b = b;
        }
        
        public override double GetArea()
        {
            return Area;
        }
    }
}