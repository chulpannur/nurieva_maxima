﻿using System;

namespace Homework_9
{
    class Program
    {
        static void Main(string[] args)
        {
            var rectangle = new Rectangle("Rectangle", 4, 5);
            rectangle.Print();
            Console.WriteLine($"S = {rectangle.GetArea()}");
            
            var triangle = new Triangle("Triangle", 3, 5);
            triangle.Print();
            Console.WriteLine($"S = {triangle.GetArea()}");
        }
    }
}