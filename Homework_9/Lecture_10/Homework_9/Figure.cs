using System;

namespace Homework_9
{
    public abstract class Figure
    {
        private string name;
        
        public string Name => name;

        public abstract double Area { get;}

        public abstract double GetArea();

        public virtual void Print()
        {
            Console.Write($"Name:{Name} ");
        }

        public Figure(string name)
        {
            this.name = name;
        }

    }
}