using System;

namespace Homework_9
{
    public class Triangle: Figure
    {
        private double a;
        
        private double h;

        public override double Area => (a * h) / 2;

        public Triangle(string name, double a, double h) : base(name)
        {
            this.a = a;
            this.h = h;
        }
        
        public override double GetArea()
        {
            return Area;
        }
    } 
} 